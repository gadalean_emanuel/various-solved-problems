package main;

public class EmiList<E> implements Lista<E> {

    private Object[] vector;

    private int length = 1;//how many objects fit in collection

    private int size = 0;//how many objects are actually in collection

    public EmiList() {
        vector = new Object[length];
    }

    @Override
    public int size() {
        if (size == 0) {
            return 0;
        } else {
            return size;
        }
    }

    @Override
    public void add(E value) {
        if (size == length) {
            increaseLengthAtNeed();
        }
        vector[size] = value;
        size++;
    }

    @Override
    public boolean delete(E value) {
        boolean foundIt = false;
        for (int i = 0; i < size; i++) {
            if (vector[i] == value) {
                foundIt = true;
                for (int j = i; j < size - 1; j++) {
                    vector[j] = vector[j + 1];
                }
                size--;
            }
        }
        return foundIt;
    }

    @Override
    public int length() {
        return length;
    }

    @Override
    public void addAtIndex(int index, E value) {
        if (index > size || index < 0) {
            throw new IndexOutOfBoundsException("Please select an index in the range: [0, size])");
        }
        increaseLengthAtNeed();
        for (int i = size - 1; i > index; i--) {
            vector[i + 1] = vector[i];
        }
        vector[index] = value;
        size++;
    }

    /**
     * create a copy of this collection with length doubled
     */
    private void increaseLengthAtNeed() {
        length *= 2;
        Object[] newVector = new Object[length];
        for (int i = 0; i < size; i++) {
            newVector[i] = vector[i];
        }
        vector = newVector;
    }

    @Override
    public String toString() {
        String s = "";
        for (int i = 0; i < size; i++) {
            s += vector[i] + " ";
        }
        return s;
    }
}
