package homework;

import java.util.Scanner;

public class DiceProblem {
    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        int diceRolls[] = {};
        System.out.println(numberOfPlayers(diceRolls));
    }


    /**
     * @param diceRolls
     * @param startIndex-first position of a 6
     * @return-the number of 6 in a row
     */
    private static int countNumberOf6InARow(int diceRolls[], int startIndex) {
        int numberOf6 = 0;
        for (int i = startIndex; i < diceRolls.length; i++) {
            if (diceRolls[i] == 6)
                numberOf6++;
            else
                break;
        }

        return numberOf6;

    }

    /**
     * @param diceRolls
     * @return: -1 -> the input is not valid
     *number of girls -> input is valid
     */
    private static int numberOfPlayers(int diceRolls[]) {
        if (diceRolls[diceRolls.length - 1] == 6)
            return -1;
        int numberOfGirls = 0;
        int i = 0;
        while (i < diceRolls.length) {
            if (diceRolls[i] != 6) {
                numberOfGirls++;
//                i++;
            }
            i++;
//            if (diceRolls[i] == 6) {
//                int jumpValue = countNumberOf6InARow(diceRolls, i) + 1;
//                numberOfGirls++;
//                i += jumpValue;
//            }

        }
        return numberOfGirls;
    }
}

