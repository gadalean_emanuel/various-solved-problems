package com.company;

public class PrimitiveUtils {

    public static <E, T> double sum(E param1, T param2) {
        double param1Value;
        double param2Value;
        if (Tools.isOneOfTheRequiredTypes(param1)) {
            if (Tools.isBooleanOrCharacter(param1)) {
                param1Value = Tools.getBooleanOrCharacterRequestValues(param1);
            } else {
                param1Value = Double.parseDouble(param1.toString());
            }
        } else {
            param1Value = System.identityHashCode(param1);
        }

        if (Tools.isOneOfTheRequiredTypes(param2)) {
            if (Tools.isBooleanOrCharacter(param2)) {
                param2Value = Tools.getBooleanOrCharacterRequestValues(param2);
            } else {
                param2Value = Double.parseDouble(param2.toString());
            }
        } else {
            param2Value = System.identityHashCode(param2);
        }

        return param1Value + param2Value;
    }

    public static <E, T> double dif(E param1, T param2) {
        double param1Value;
        double param2Value;
        if (Tools.isOneOfTheRequiredTypes(param1)) {
            if (Tools.isBooleanOrCharacter(param1)) {
                param1Value = Tools.getBooleanOrCharacterRequestValues(param1);
            } else {
                param1Value = Double.parseDouble(param1.toString());
            }
        } else {
            param1Value = System.identityHashCode(param1);
        }

        if (Tools.isOneOfTheRequiredTypes(param2)) {
            if (Tools.isBooleanOrCharacter(param2)) {
                param2Value = Tools.getBooleanOrCharacterRequestValues(param2);
            } else {
                param2Value = Double.parseDouble(param2.toString());
            }
        } else {
            param2Value = System.identityHashCode(param2);
        }

        return param1Value - param2Value;
    }

    public static <E, T> String toString(E param1, T param2) {
        return param1.toString() + " " + param2.toString();
    }

    public static <E> Class<?> getReturnType(E param1) {
        return param1.getClass();

    }
}
