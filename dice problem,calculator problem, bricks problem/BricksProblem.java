package homework;

import java.util.Scanner;

public class BricksProblem {
    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        String[] boysName = {"Patlu", "Motu"};
        System.out.println("Introduceti numarul de caramizi: ");
        int bricks = readInt();
        try {
            System.out.println("Cel care pune ultima caramida este: " + boysName[whoFinishFirst(bricks)]);
        } catch (IllegalArgumentException e) {
            System.out.println("Numarul de caramizi trebuie sa fie mai mare ca 0");
        }
    }

    /**
     * In each round we add up how many bricks Patlu puts respectively Motlu;
     * After adding one of them bricks to our amount,we check if amount is bigger or equal to numberOfBricks that is given
     *
     * @param numberOfBricks
     * @throws IllegalArgumentException - number of bricks < 1
     * @return: 0 - if Patlu puts the last brick
     * 1 - if Motu puts the last brick
     */
    private static int whoFinishFirst(int numberOfBricks) throws IllegalArgumentException {
        if (numberOfBricks < 1)
            throw new IllegalArgumentException();
        int numberOfBricksPerRound = 0;
        int round = 1;
        while (numberOfBricksPerRound <= numberOfBricks) {
            int patluBriks = round;
            numberOfBricksPerRound = numberOfBricksPerRound + patluBriks;
            if (numberOfBricksPerRound >= numberOfBricks)
                return 0; //patlu wins
            int motluBriks = 2 * patluBriks;
            numberOfBricksPerRound = numberOfBricksPerRound + motluBriks;
            if (numberOfBricksPerRound >= numberOfBricks)
                return 1; //motlu wins
            round++;
        }
        return -1;
    }

    private static int readInt() {
        int num = scan.nextInt();
        return num;
    }

}
