package com.company;

public class Minime {

    public static void main(String[] args) {
        int array[] = {Integer.MAX_VALUE, Integer.MAX_VALUE};
        try {
            System.out.println(min(array));
        } catch (IllegalArgumentException e) {
            System.out.println("Mai baga o fisa");
        }
    }


    private static long min(int array[]) throws IllegalArgumentException {
        int mini1 = Integer.MAX_VALUE;
        int mini2 = Integer.MAX_VALUE;
        if (array.length == 1) {
            throw new IllegalArgumentException();
        }
        for (int i = 0; i < array.length; i++) {
            if (array[i] <= mini1) {
                mini2 = mini1;
                mini1 = array[i];
            } else if (array[i] <= mini2) {
                mini2 = array[i];
            }
        }
        return (long)mini1 + mini2;
//            System.out.println("Cea mai mica valoare este: " + mini1 + ", urmata de " + mini2);
    }


}
