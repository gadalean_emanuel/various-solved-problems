package com.company;

import java.util.Scanner;

public class Binarysearch {
    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Baga numaru de elemente:");
        int nre = readInt();
        int v[] = new int[nre];
        for (int i = 0; i < nre; i++) {
            System.out.print("v[" + i + "]= ");
            v[i] = readInt();
        }

        int element = 7;
        if (BinarySearch(v, element) == 1)
            System.out.println("Am gasit");
        else
            System.out.println("Nu este");

    }

    private static int BinarySearch(int v[], int element) {
        int stanga = 0;
        int dreapta = v.length - 1;
        int mijloc;
        while (stanga <= dreapta) {
            mijloc = (stanga + dreapta) / 2;
            if (element == v[mijloc])
                return 1;
            else if (element > v[mijloc])
                stanga = mijloc + 1;
            else
                dreapta = mijloc - 1;
        }
        return 0;
    }

    private static int readInt() {
        int num = scan.nextInt();
        return num;
    }
}
