package enum_court;

import java.util.ArrayList;
import java.util.List;

public class Formatting {
    /**
     * this method format one court(String)
     *
     * @param court - String
     * @return - court(@param) formatted
     */
    public static String courtFormatting(String court) {
        if (court.contains("CURTEA_MILITARADE_APEL")) {
            court = "CURTEA MILITARA DE APEL " + cityFormatting(court.substring(23));
        } else if (court.contains("CURTEADE_APEL")) {
            court = "CURTEA DE APEL " + cityFormatting(court.substring(14));
        } else if (court.contains("TRIBUNALULPENTRUMINORI_SIFAMILIE")) {
            court = "TRIBUNALUL PENTRU MINORI SI FAMILIE " + cityFormatting(court.substring(33));
        } else {
            int lastUnderscore = court.lastIndexOf("_");
            court = court.replaceAll("_", " ");
            court = court.substring(0, lastUnderscore + 1) + cityFormatting(court.substring(lastUnderscore + 1));
        }
        return court;
    }


    public static String cityFormatting(String court) {
        if (court.contains("TARGU")) {
            return "TARGU " + court.substring(5);
        } else if (court.contains("ALBA")) {
            return "ALBA IULIA";
        } else if (court.contains("SATU")) {
            return "SATU-MARE";
        } else if (court.contains("BISTRITA")) {
            return "BISTRITA-NASAUD";
        } else if (court.contains("CARAS")) {
            return "CARAS-SEVERIN";
        } else if (court.contains("BAIADEARAMA")) {
            return "BAIA-DE-ARAMA";
        } else if (court.contains("BOLINTIN")) {
            return "BOLINTIN-VALE";
        } else if (court.contains("CAMPULUNGM")) {
            return "CAMPULUNG MOLDOVENESC";
        } else if (court.contains("CLUJNAPOCA")) {
            return "CLUJ-NAPOCA";
        } else if (court.contains("CURTEADEARGES")) {
            return "CURTEA DE ARGES";
        } else if (court.contains("DROBETA")) {
            return "DROBETA-TURNU SEVERIN";
        } else if (court.contains("GURAHUMORULUI")) {
            return "GURA HUMORULUI";
        } else if (court.contains("INTORSURABUZAULUI")) {
            return "INTORSURA BUZAULUI";
        } else if (court.contains("MIERCUREACIUC")) {
            return "MIERCUREA CIUC";
        } else if (court.contains("ODORHEIUL")) {
            return "ODORHEIUL SECUIESC";
        } else if (court.contains("PIATRANEAMT")) {
            return "PIATRA-NEAMT";
        } else if (court.contains("RAMNICU")) {
            return "RAMNICU " + court.substring(7);
        } else if (court.contains("ROAIORIDEVERDE")) {
            return "ROSIORI DE VERDE";
        } else if (court.contains("TURNUMAGURELE")) {
            return "TURNU MAGURELE";
        } else if (court.contains("VALENII")) {
            return "VALENII DE MUNTE";
        } else if (court.contains("VISEU")) {
            return "VISEU DE SUS";
        } else if (court.contains("SIGHETU")) {
            return "SIGHETU MARMATIEI";
        } else if (court.contains("SIMLEUL")) {
            return "SIMLEUL SILVANIEI";
        } else if (court.contains("SFANTUGHEORGHE")) {
            return "SFANTU GHEORGHE";
        } else if (court.contains("VATRADORNEI")) {
            return "VATRA-DORNEI";
        } else if (court.contains("VANJUMARE")) {
            return "VANJU MARE";
        } else if (court.contains("PODUTURCULUI")) {
            return "PODU TURCULUI";
        } else if (court.contains("SOMCUTAMARE")) {
            return "SOMCUTA MARE";
        } else if (court.contains("BAIAMARE")) {
            return "BAIA-MARE";
        } else if (court.contains("CHISINEUCRIS")) {
            return "CHISINEU-CRIS";
        } else if (court.contains("LEHIUGARA")) {
            return "LEHLIU-GARA";
        } else if (court.contains("MOLDOVANOUA")) {
            return "MOLDOVA NOUA";
        } else if (court.contains("NEGRESTIOAS")) {
            return "NEGRESTI-OAS";
        } else if (court.contains("ROSIORIDEVEDE")) {
            return "ROSIORI DE VEDE";
        } else if (court.contains("SANNICOLAULMARE")) {
            return "SANNICOLAUL MARE";
        }
        return court;
    }

    /**
     * @param courtList - array with courts from Court(enum)
     * @return - courtList(@param) with formatted courts, the courts are in a String form
     */
    public static List<String> courtFormat(Court[] courtList) {
        List<String> formattedCourts = new ArrayList<>();
        for (Court court : courtList) {
            formattedCourts.add(courtFormatting(court.toString()));
        }
        return formattedCourts;
    }
}
