package main;

/**
 * A custom version of List from java.util
 * @param <E> - the type of elements in the list
 */
public interface Lista<E> {

    /**
     * @return - the number of objects that are in collection
     */
    int size();

    /**
     * this method add an object at the end of the collection
     * the size of the collection will be doubled if there is not enough space for our item
     *
     * @param value - generic object
     */
    void add(E value);

    /**
     *
     * @param value - generic object
     * @return - boolean value(true -> the value(@param) was successfully deleted; false -> the value does not exist in collection, deletion was not performed)
     */
    boolean delete(E value);

    /**
     *
     * @return - length of this collection(how many objects can fit in)
     */
    int length();

    /**
     * this method add a specific object at index(@param) in collection
     * the size of the collection will be doubled if there is not enough space for our item
     * @param index - position in our collection(an exception will be thrown in case that index is out of bounds)
     * @param value - generic object
     */
    void addAtIndex(int index, E value);
}
