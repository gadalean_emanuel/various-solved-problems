package com.company;

public class SumaElemente {

    public static void main(String[] args) {
        int v[] = {999999999, 3};
        System.out.println("Cea mai mica suma este " + suma(v));
    }

    private static int suma(int v[]) {
        int sumMin = Integer.MAX_VALUE;
        int sum;
        for (int i = 0; i < v.length - 1; i++) {
            for (int j = i + 1; j < v.length; j++) {
                sum = v[i] + v[j];
                if (sum < sumMin) {
                    sumMin = sum;
                }
            }
        }
        return sumMin;
    }

}
