package com.company;

public class Tools {

    public static <E> boolean isOneOfTheRequiredTypes(E value) {
        if (value.getClass() == Integer.class || value.getClass() == Double.class || value.getClass() == Float.class) {
            return true;
        } else return value.getClass() == Boolean.class || value.getClass() == Character.class;
    }

    public static <E> double getBooleanOrCharacterRequestValues(E value) {
        if (value.getClass() == Character.class) {
            return value.toString().codePointAt(0);
        } else {
            return getBooleanValue((Boolean) value);
        }
    }

    public static <E> boolean isBooleanOrCharacter(E value) {
        return value.getClass() == Boolean.class || value.getClass() == Character.class;
    }

    public static int getBooleanValue(boolean variable) {
        if (variable) return 1;
        return 0;
    }

}
