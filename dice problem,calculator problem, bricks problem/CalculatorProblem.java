package homework;

import java.util.ArrayList;
import java.util.Scanner;


public class CalculatorProblem {
    static Scanner scan = new Scanner(System.in);

    static ArrayList<Character> operations = new ArrayList<>() {{
        add('+');
        add('-');
        add('*');
        add('/');
        add('%');
    }};

    public static void main(String[] args) {
        System.out.println("Introduceti primul numar: ");
        int x = readInt();
        System.out.println("Introduceti al doilea numar: ");
        int y = readInt();
        System.out.println("Introduceti caracterul: ");
        char sign = read();

        try {
            System.out.println("output: " + compute(x, y, sign));
        } catch (IllegalArgumentException e) {
            System.out.println("Ati introdus un caracter necunoscut.Va rugam reincercati!");
        }
    }


    /**
     * @param firstNumber
     * @param secondNumber
     * @param sign-the     given character
     * @throws IllegalArgumentException-if the given character is not in our array
     */

    private static float compute(int firstNumber, int secondNumber, char sign) throws IllegalArgumentException {
        if (!operations.contains(sign)) throw new IllegalArgumentException();
        if (sign == '+') return (long) firstNumber + secondNumber;
        if (sign == '-') return firstNumber - secondNumber;
        if (sign == '*') return (long) firstNumber * secondNumber;
        if (sign == '/') return (float) firstNumber / secondNumber;
        if (sign == '%') return firstNumber % secondNumber;
        return 0;
    }

    private static int readInt() {
        int num = scan.nextInt();
        return num;
    }


    private static char read() {
        return scan.next().charAt(0);
    }

}


