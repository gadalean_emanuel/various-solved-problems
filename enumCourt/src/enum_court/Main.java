package enum_court;

import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import static enum_court.Formatting.*;

public class Main {
    //TODO format orase tote cu diacritice
    public static void main(String[] args) {
        List<String> courts = courtFormat(Court.values());

        while (true) {
            System.out.println("""
                    1.Lista cu instantele formatate!
                    2.Formateaza o instanta dorita!
                    3.EXIT!""");
            int userOption = readInt();
            switch (userOption) {
                case 1:
                    for (String court : courts) {
                        System.out.println(court);
                    }
                    System.out.println();
                    break;

                case 2:
                    System.out.println("Introduceti o instanta de judecata:");
                    String readCourt = readString().toUpperCase(Locale.ROOT);
                    System.out.println("Instanta formatata: " + courtFormatting(readCourt) + "\n");
                    break;

                case 3:
                    return;
            }
        }

    }

    public static String readString() {
        Scanner scan = new Scanner(System.in);
        return scan.nextLine();
    }

    public static int readInt() {
        Scanner scan = new Scanner(System.in);
        return scan.nextInt();
    }

}

